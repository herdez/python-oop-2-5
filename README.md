
# Instance Variables Challenge

## Define classes

Define classes for both the `Driver` and `Passenger` classes.


```python
# Driver class

```


```python
# Passenger class

```

Instantiate a new instance of a passenger and a new instance of a driver. Tests must be `True`. 


```python
# Assign a driver instance


# Give the driver instance object 'miles_driven' of 100000

```


```python
# Assign a passenger instance


# Give the passenger instance object a 'rating' of 4.9

```


```python
# driver code
print(isinstance(driver, Driver) == True)
print(isinstance(passenger, Passenger) == True)
print(passenger.rating == 4.9)
print(driver.miles_driven == 100000)
```

    True
    True
    True
    True


## Search attributes using functions 

Build a function to find a driver with a given name. The function should take two inputs, `drivers` and `search_name`. 

Tests must be **True**. **Warning**: It is a must, **list comprehension** and **refactored code** for two code lines.


```python
# define find_driver_by_name function

```

Create **drivers** with relevant attributes. All tests must be **True**. 


```python
# Create drivers with relevant attributes



# driver code
print('alex_driver is Driver class instance?:    ', isinstance(alex_driver, Driver) == True)
print('alex_driver has name?:                    ', hasattr(alex_driver, 'name'))
print('alex_driver name is alex?:                ', True if alex_driver.name == "alex" else False if hasattr(alex_driver, 'name') else False)
print('alex_driver has rating?:                  ', hasattr(alex_driver, 'rating'))
print('alex_driver rating is 9.0?:               ', True if alex_driver.rating == 9.0 else False if hasattr(alex_driver, 'rating') else False)
print("-" * 5)
print('michelle_driver is Driver class instance?:', isinstance(michelle_driver, Driver) == True)
print('michelle_driver has name?:                ', hasattr(michelle_driver, 'name'))
print('michelle_driver name is michelle?:        ', True if michelle_driver.name == "michelle" else False if hasattr(michelle_driver, 'name') else False)
print('michelle_driver has rating?:              ', hasattr(michelle_driver, 'rating'))
print('michelle_driver rating is 8.0?:           ', True if michelle_driver.rating == 8.0 else False if hasattr(michelle_driver, 'rating') else False)
print("-" * 5)
print('jake_driver is Driver class instance?:    ', isinstance(jake_driver, Driver) == True)
print('jake_driver has name?:                    ', hasattr(jake_driver, 'name'))
print('jake_driver name is jake?:                ', True if jake_driver.name == "jake" else False if hasattr(jake_driver, 'jake') else False)
print('jake_driver has rating?:                  ', hasattr(jake_driver, 'rating'))
print('jake_driver rating is 9.7?:               ', True if jake_driver.rating == 9.7 else False if hasattr(jake_driver, 'rating') else False)
print("-" * 5)
print('ashleigh_driver is Driver class instance?:', isinstance(ashleigh_driver, Driver) == True)
print('ashleigh_driver has name?:                ', hasattr(ashleigh_driver, 'name'))
print('ashleigh_driver name is ashleigh?:        ', True if ashleigh_driver.name == "ashleigh" else False if hasattr(ashleigh_driver, 'ashleigh') else False)
print('ashleigh_driver has rating?:              ', hasattr(ashleigh_driver, 'rating'))
print('ashleigh_driver rating is 8.75?:          ', True if ashleigh_driver.rating == 8.75 else False if hasattr(ashleigh_driver, 'rating') else False)
print("-" * 5)
print('list_of_drivers is lenght 4?:             ', (len(list_of_drivers) if type(list_of_drivers) == list else False) == 4)
```

    alex_driver is Driver class instance?:     True
    alex_driver has name?:                     True
    alex_driver name is alex?:                 True
    alex_driver has rating?:                   True
    alex_driver rating is 9.0?:                True
    -----
    michelle_driver is Driver class instance?: True
    michelle_driver has name?:                 True
    michelle_driver name is michelle?:         True
    michelle_driver has rating?:               True
    michelle_driver rating is 8.0?:            True
    -----
    jake_driver is Driver class instance?:     True
    jake_driver has name?:                     True
    jake_driver name is jake?:                 True
    jake_driver has rating?:                   True
    jake_driver rating is 9.7?:                True
    -----
    ashleigh_driver is Driver class instance?: True
    ashleigh_driver has name?:                 True
    ashleigh_driver name is ashleigh?:         True
    ashleigh_driver has rating?:               True
    ashleigh_driver rating is 8.75?:           True
    -----
    list_of_drivers is lenght 4?:              True


Test `find_driver_by_name()`. Tests must be **True**.


```python
# Find "jake"

# driver code
output_1 == jake_driver
```




    True




```python
# Find "michelle"

# driver code
output_2 == michelle_driver
```




    True




```python
# Find "allison"

# driver code
output_3 == "Sorry, we couldn't find a driver with the name, Allison! :("
```




    True




```python
# Find Charly

# driver code
output_4 == "Sorry, we couldn't find a driver with the name, Charly! :("
```




    True



Define `name_starts_with` function that returns the list of instance objects whose name starts with a given substring. Test your function, all tests must be **True**.


```python
# Define your function here 

```


```python
# Find all drivers whose name start with 'a'

# driver code
def sort_lists_by_name(list):
    return sorted(name_starts_with(list, 'a'), key=lambda driver: driver.name)

print(sort_lists_by_name(name_starts_with(list_of_drivers, 'a')) == [alex_driver, ashleigh_driver])
print(sort_lists_by_name(list_of_drivers)[1].name == 'ashleigh')
print(name_starts_with(list_of_drivers, 'al') == [alex_driver])
print(name_starts_with(list_of_drivers, 'az') == [])
```

    True
    True
    True
    True


Define a function that returns the instance object of the driver with the highest rating. It is a must **lambda expression** and **list comprehension**. Test must be **True**. 


```python
# Write your function here that returns the driver with the highest rating

```


```python
# Test out highest_rated_driver() to find the driver with the highest rating
 
# Define a list of four drivers


# driver code
def create_mory_driver_instance():
    mory_driver = Driver()
    mory_driver.name = "mory"
    mory_driver.rating = 10.0
    return mory_driver


mory = create_mory_driver_instance()
list_of_drivers.append(mory)
print(highest_rated_driver(list_of_drivers) is mory) 
```

    True


## Instance Method

Define a `NewDriver` class with an instance method called `passenger_names()`. This method accesses the `passengers` attribute of the class and returns a list of all `name`s associated with `passengers`. 


```python
# Define the NewDriver class

```

Create **passengers** with name attribute. All tests must be **True**. 


```python
# Passengers 



# driver code
print('alex_passenger is Passenger class instance?:    ', isinstance(alex_passenger, Passenger) == True)
print('alex_passenger has name?:                       ', hasattr(alex_passenger, 'name'))

print('michelle_passenger is Passenger class instance?:', isinstance(michelle_passenger, Passenger) == True)
print('michelle_passenger has name?:                   ', hasattr(michelle_passenger, 'name'))

print('jake_passenger is Passenger class instance?:    ', isinstance(jake_passenger, Passenger) == True)
print('jake_passenger has name?:                       ', hasattr(jake_passenger, 'name'))

print('ashleigh_passenger is Passenger class instance?:', isinstance(ashleigh_passenger, Passenger) == True)
print('ashleigh_passenger has name?:                   ', hasattr(ashleigh_passenger, 'name'))

print('list_of_passengers is lenght 4?:                ', (len(list_of_passengers) if type(list_of_passengers) == list else False) == 4)
```

    alex_passenger is Passenger class instance?:     True
    alex_passenger has name?:                        True
    michelle_passenger is Passenger class instance?: True
    michelle_passenger has name?:                    True
    jake_passenger is Passenger class instance?:     True
    jake_passenger has name?:                        True
    ashleigh_passenger is Passenger class instance?: True
    ashleigh_passenger has name?:                    True
    list_of_passengers is lenght 4?:                 True


Now, instantiate a `NewDriver` class called `best_driver`. All Tests must be **True**: 


```python
# Instantiate a NewDriver class object
best_driver = NewDriver()

# driver code
print(hasattr(best_driver, "name"))
print(hasattr(best_driver, "car_make"))
print(hasattr(best_driver, "car_model"))
print(hasattr(best_driver, "age"))
print(hasattr(best_driver, "passengers"))


# Add the name attribute and assign it 'Garol'


# Add the car_make attribute and assign it 'toyota'


# Add the car_model attribute and assign it 'camry'


# Add the age attribute and assign it 30


# Add the passengers attribute and assign it to list_of_passengers



#driver code
print("\n")
print(type(best_driver.name) == str)
print(type(best_driver.car_make) == str)
print(type(best_driver.car_model) == str)
print(type(best_driver.age) == int)
print(len(best_driver.passengers) == 4)
```

    True
    True
    True
    True
    True
    
    
    True
    True
    True
    True
    True


You already has assigned some passengers to this **driver**. What are their names? Test must be **True**.  


```python
# Find the names of the passengers with passenger_names() 

# driver code
print(names_of_passengers == ['alex', 'michelle', 'jake', 'ashleigh'])
```

    True

